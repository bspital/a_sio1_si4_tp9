package exercices;


import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import  static utilitaires.UtilDojo.categories;

public class Question02 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartCagtegPoids= new TreeMap(); 
     
      
      // pour vous aider !
      
      // on accède à toutes les catégories de poids comme cela:
      
      
      for (String cat :categories ){
      
          repartCagtegPoids.put(cat,0);
      }
      
      
      
      System.out.println("\nNombre de personnes par catégories de poids\n");

      
      // A completer
      
      for ( Personne pers : listeDesPersonnes ){
        int val = repartCagtegPoids.get(utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids ))+1;
        repartCagtegPoids.put(utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids ),val);
     }
      
      for( Entry  e: repartCagtegPoids.entrySet()){
            System.out.printf("%-15s %3d\n",e.getKey(),e.getValue() );
      }
      
  }
}


