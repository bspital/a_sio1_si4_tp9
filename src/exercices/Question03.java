package exercices;


import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import static utilitaires.UtilDojo.categories;

public class Question03 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartCagtegPoids= new TreeMap(); 
      
      System.out.println("\nCatégories de poids comportant 3 personnes ou plus \n");
       
      for (String cat :categories ){
      
          repartCagtegPoids.put(cat,0);
      }
            
      // A completer
      
      for ( Personne pers : listeDesPersonnes ){
        int val = repartCagtegPoids.get(utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids ))+1;
        repartCagtegPoids.put(utilitaires.UtilDojo.determineCategorie( pers.sexe, pers.poids ),val);
     }
      
      for( String  e: repartCagtegPoids.keySet()){
         if(repartCagtegPoids.get(e) >= 3) {
            System.out.printf("%-15s %3d\n",e,repartCagtegPoids.get(e) );
         }
      }
   }
}



