package exercices;


import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import static utilitaires.UtilDojo.categories;

public class Question01 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartVille= new TreeMap(); 
     
      System.out.println("\nNombre de personnes par villes\n");
      // A completer
      
      for ( Entry ville : repartVille.entrySet()) {
           System.out.printf("%-15s %2d\n",ville.getKey(),ville.getValue());
      }
      
      for (Personne pers : listeDesPersonnes ){
      
          repartVille.put(pers.ville,0);
      }
      
      
      
      System.out.println("\nNombre de personnes par catégories de poids\n");

      
      // A completer
      
      for ( Personne pers : listeDesPersonnes ){
        int val = repartVille.get(pers.ville)+1;
        repartVille.put(pers.ville,val);
     }
      
      for( Entry  e: repartVille.entrySet()){
            System.out.printf("%-15s %3d\n",e.getKey(),e.getValue() );
      }
  
  }
}