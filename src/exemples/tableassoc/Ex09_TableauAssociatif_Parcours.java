
package exemples.tableassoc;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ex09_TableauAssociatif_Parcours {
   
    public static void main(String[] args) {
       
        // Déclaration du tableau associatif clé de type String valeur de type Integer
        
        Map<String,Integer> resultats=new TreeMap();
        
        
        resultats.put("Pierre", 12);
        resultats.put("Alain", 10);
        resultats.put("Jacques", 8);
        resultats.put("Fabienne", 16);
        resultats.put("Marie", 13);
        resultats.put("Béatrice", 14);
        resultats.put("Léa", 15);
        resultats.put("Thierry", 16);
         
        System.out.println();
        
        
        // Liste des clés
        // Nous allons parcourir est afficher la liste des clés du tableau associatif resultats
        // la liste des clés s'obtient avec la syntaxe: resultats.keySet()

        System.out.println("Liste des clés\n");
        
        
        // On retrouve le parcours de liste FOR EACH déjà utilisé mainte fois
        // pour parcourir la liste: resultats.keySet()
        
        for ( String nom : resultats.keySet()) {
        
           System.out.print(resultats.get(nom) + "  ");   
        }
        
        System.out.println("\n");
        
        // Liste des valeurs
         // Nous allons parcourir est afficher la liste des valeurs du tableau associatif resultats
        // la liste des clés s'obtient avec la syntaxe: resultats.values()
        
        System.out.println("Liste des valeurs\n");
        
          // On retrouve le parcours de liste FOR EACH
        for ( Integer note :resultats.values()) {
        
           System.out.print(note+ " ");   
        }
        
        System.out.println("\n");
        
        
        // Liste des entrées
        
        System.out.println("Liste des entrées\n");
        
        
        // On obtient la Liste des entrées 
        // avec la syntaxe: resultats.entrySet()
        // Chaque entrée est du type Entry
        
        // boucle FOR EACH
        
        // pour chaque elelment de type Entry
        // repéré par la variable ent
        // dans  ( : ) la collection resultats.entrySet()
        // on effectue le trairement situé entre les deux accolades { ...........}
        
        for ( Entry ent : resultats.entrySet()) {
        
           // ent.getKey()    donne la clé de l'entrée
           // ent.getValue()  donne la valeur de l'entrée
           // Nous affichons ces deux informations  pour chaque entrée du tableau associatif resultats 
        
            System.out.println(ent.getKey()+ " "+ ent.getValue());   
        }
        
        System.out.println("\n");
        
        
    }
}
